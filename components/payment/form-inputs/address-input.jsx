import React, { useEffect, useState } from "react";

const AddressInput = ({
  value,
  setValue,
  secondValue,
  setSecondValue,
  onIsAddressValid,
  label,
  placeholder,
  optionalPlaceholder,
}) => {
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    setIsValid(value.trim() !== "");
  }, [value]);

  useEffect(() => {
    onIsAddressValid(isValid);
  }, [isValid]);

  return (
    <div className="mb-3">
      <label htmlFor="address_input" className="form-label">
        {label}
      </label>
      <input
        type="text"
        className={`form-control mb-3 ${isValid ? "is-valid" : "is-invalid"}`}
        id="address_input"
        placeholder={placeholder}
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <input
        type="text"
        className="form-control"
        placeholder={optionalPlaceholder}
        value={secondValue}
        onChange={(e) => setSecondValue(e.target.value)}
      />
    </div>
  );
};

export default AddressInput;
