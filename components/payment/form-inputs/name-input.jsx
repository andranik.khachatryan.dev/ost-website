import React, { useState, useEffect } from "react";

const NameInput = ({ value, setValue, onIsNameValid, label, placeholder }) => {
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    setIsValid(value.trim() !== "");
  }, [value]);

  useEffect(() => {
    onIsNameValid(isValid);
  }, [isValid]);

  return (
    <div className="mb-3">
      <label htmlFor="name_input" className="form-label">
        {label}
      </label>
      <input
        type="email"
        className={`form-control ${isValid ? "is-valid" : "is-invalid"}`}
        id="name_input"
        placeholder={placeholder}
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
    </div>
  );
};

export default NameInput;
