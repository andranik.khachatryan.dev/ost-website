import React from "react";

const DetailsInput = ({ value, setValue, title, label }) => {
  return (
    <div>
      <h5>{title}</h5>
      <label htmlFor="textarea-input" className="form-label">
        {label}
      </label>
      <textarea
        className="form-control"
        id="textarea-input"
        rows="6"
        value={value}
        onChange={(e) => setValue(e.target.value)}
      ></textarea>
    </div>
  );
};

export default DetailsInput;
