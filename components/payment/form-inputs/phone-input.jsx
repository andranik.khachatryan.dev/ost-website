import React, { useState, useEffect } from "react";

const PhoneInput = ({
  value,
  setValue,
  secondValue,
  setSecondValue,
  onIsPhoneValid,
  label,
  primaryPlaceholder,
  secondaryPlaceholder,
}) => {
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    if (value.trim() === "" || value.length < 9) {
      setIsValid(false);
    } else {
      setIsValid(true);
    }
  }, [value]);

  useEffect(() => {
    onIsPhoneValid(isValid);
  }, [isValid]);

  return (
    <div className="mb-3">
      <label htmlFor="phoe_input" className="form-label">
        {label}
      </label>
      <input
        type="number"
        className={`form-control mb-3 ${isValid ? "is-valid" : "is-invalid"}`}
        id="phoe_input"
        placeholder={primaryPlaceholder}
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <input
        type="number"
        className="form-control"
        placeholder={secondaryPlaceholder}
        value={secondValue}
        onChange={(e) => setSecondValue(e.target.value)}
      />
    </div>
  );
};

export default PhoneInput;
