import React from "react";

const PaymentMethodInput = ({
  paymentMethod,
  handleInputChange,
  title,
  optionCash,
  optionCredit,
}) => {
  return (
    <div className="mb-3">
      <h5>{title}</h5>
      <div className="form-check">
        <input
          className="form-check-input"
          type="radio"
          name="payment-method"
          id="cashe"
          value="cashe"
          checked={paymentMethod === "cashe"}
          onChange={handleInputChange}
        />
        <label className="form-check-label" htmlFor="flexRadioDefault1">
          {optionCash}
        </label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="radio"
          name="payment-method"
          id="credit-card"
          value="credit-card"
          checked={paymentMethod === "credit-card"}
          onChange={handleInputChange}
        />
        <label className="form-check-label" htmlFor="flexRadioDefault2">
          {optionCredit}
        </label>
      </div>
    </div>
  );
};

export default PaymentMethodInput;
