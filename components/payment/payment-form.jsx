import React, { useEffect, useState } from "react";
import NameInput from "./form-inputs/name-input";
import PhoneInput from "./form-inputs/phone-input";
import AddressInput from "./form-inputs/address-input";
import PaymentMethodInput from "./form-inputs/pay-method-input";
import DetailsInput from "./form-inputs/details-input";
import { useTranslation } from "next-i18next";

const PaymentForm = ({ onGetData }) => {
  const { t } = useTranslation("payment");

  const [name, setName] = useState("");
  const [isNameValid, setIsNameValid] = useState(false);
  const [phonePrimary, setPhonePrimary] = useState("");
  const [isPhoneValid, setIsPhoneValid] = useState("");
  const [phoneSecondary, setPhoneSecondary] = useState("");
  const [address, setAddress] = useState("");
  const [isAddressValid, setIsAddressValid] = useState(false);
  const [addressOptional, setAddressOptional] = useState("");
  const [paymentMethod, setPaymentMethod] = useState("cashe");
  const [details, setDetails] = useState("");

  function handleInputChange(event) {
    setPaymentMethod(event.target.value);
  }

  useEffect(() => {
    const data = {
      name,
      phonePrimary,
      phoneSecondary,
      address,
      addressOptional,
      paymentMethod,
      details,
    };
    onGetData({
      data,
      isNameValid,
      isPhoneValid,
      isAddressValid,
    });
  }, [
    name,
    phonePrimary,
    phoneSecondary,
    address,
    addressOptional,
    paymentMethod,
    details,
    isNameValid,
    isPhoneValid,
    isAddressValid,
  ]);

  return (
    <form>
      <NameInput
        value={name}
        setValue={setName}
        onIsNameValid={setIsNameValid}
        label={t("name-label")}
        placeholder={t("name-placeholder")}
      />
      <PhoneInput
        value={phonePrimary}
        setValue={setPhonePrimary}
        secondValue={phoneSecondary}
        setSecondValue={setPhoneSecondary}
        onIsPhoneValid={setIsPhoneValid}
        label={t("phone-label")}
        primaryPlaceholder={t("phone-primary-placeholder")}
        secondaryPlaceholder={t("phone-secondary-placeholder")}
      />
      <AddressInput
        value={address}
        setValue={setAddress}
        secondValue={addressOptional}
        setSecondValue={setAddressOptional}
        onIsAddressValid={setIsAddressValid}
        label={t("address-label")}
        placeholder={t("address-placeholder")}
        optionalPlaceholder={t("address-optional-placeholder")}
      />
      <hr />
      <PaymentMethodInput
        paymentMethod={paymentMethod}
        handleInputChange={handleInputChange}
        title={t("payment-method-text")}
        optionCash={t("payment-by-cash")}
        optionCredit={t("payment-by-credit")}
      />
      <hr />
      <DetailsInput value={details} setValue={setDetails} title={t("details-text")} label={t("details-label")}/>
    </form>
  );
};

export default PaymentForm;
