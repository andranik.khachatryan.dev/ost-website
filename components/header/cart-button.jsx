import React, { useState, useEffect } from "react";
import { useCartContext } from "@/store/cart-context.jsx";
import { useTranslation } from "next-i18next";
import Link from "next/link";

const CartButton = () => {
  const { t } = useTranslation("header");
  const { items } = useCartContext();
  const [count, setCount] = useState();

  useEffect(() => {
    const numOfCartItems = items.reduce((curNumber, item) => {
      return curNumber + item.amount;
    }, 0);
    setCount(numOfCartItems);
  }, [items]);

  return (
    <Link
      href="/cart"
      className="btn btn-outline-warning border-2 rounded-pill position-relative me-2"
    >
      {t('cart-text')}
      {count > 0 && (
        <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
          {count}
        </span>
      )}
    </Link>
  );
};

export default CartButton;
