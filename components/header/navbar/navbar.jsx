import React from "react";
import Link from "next/link";
import { useTranslation } from "next-i18next";


const Navbar = ({ column, toggleMenu }) => {
  const { t } = useTranslation("header");

  const onMenuToggle = () => {
    if (!column) {
      return;
    }
    toggleMenu();
  };

  return (
    <nav className={`nav ${column ? "flex-column" : "d-none"} d-lg-flex`}>
      <Link
        className="nav-link"
        scroll={false}
        href="/#hero"
        aria-label="Go to home page"
        onClick={onMenuToggle}
      >
        {t('home-link-text')}
      </Link>
      <Link
        className="nav-link"
        scroll={false}
        href="/products-category"
        aria-label="Go to home page"
        onClick={onMenuToggle}
      >
        {t('menu-link-text')}
      </Link>
      <Link
        className="nav-link"
        scroll={false}
        href="/#about"
        aria-label="Go to about page"
        onClick={onMenuToggle}
      >
        {t('about-link-text')}
      </Link>
      <Link
        className="nav-link"
        scroll={false}
        href="/#contact"
        aria-label="Go to contact page"
        onClick={onMenuToggle}
      >
        {t('contact-link-text')}
      </Link>
      <Link
        className="nav-link"
        scroll={false}
        href="/#gallery"
        aria-label="Read more about Practice Areas"
        onClick={onMenuToggle}
      >
        {t('gallery-link-text')}
      </Link>
      <Link
        className="nav-link"
        scroll={false}
        href="/liked"
        aria-label="Read more about Practice Areas"
        onClick={onMenuToggle}
      >
        {t('liked-link-text')}
      </Link>
    </nav>
  );
};

export default Navbar;
