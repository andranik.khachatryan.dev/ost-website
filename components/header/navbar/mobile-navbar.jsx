import Navbar from "./navbar.jsx";
import { IoClose } from "react-icons/io5";
import { useTranslation } from "next-i18next";

function NavbarMobile({ showMenu, toggleMenu }) {
  const { t } = useTranslation("header");
  return (
    <div
      className={`offcanvas offcanvas-start bg-brown mw-80 ${
        showMenu ? "show" : ""
      }`}
      tabIndex="-1"
    >
      <div className="offcanvas-header">
        <h5 className="offcanvas-title text-warning">
          {t("mobile-header-title")}
        </h5>
        <button
          aria-label="Close menu"
          onClick={toggleMenu}
          className="btn text-warning border-0 p-2"
        >
          <IoClose className="fs-4" />
        </button>
      </div>
      <div className="offcanvas-body">
        <Navbar column={true} toggleMenu={toggleMenu} />
      </div>
    </div>
  );
}

export default NavbarMobile;
