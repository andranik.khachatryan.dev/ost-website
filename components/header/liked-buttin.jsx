import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useLikedContext } from "@/store/liked-context";
import { BsFillHeartFill } from "react-icons/bs";
const LikedButton = () => {
  const { items } = useLikedContext();
  const [count, setCount] = useState();

  useEffect(() => {
    const numOfItems = items.length;
    setCount(numOfItems);
  }, [items]);

  return (
    <Link
      href="/liked"
      className="btn btn-outline-danger border-2 rounded-pill me-2 position-relative"
    >
      <BsFillHeartFill className="fs-6"/>
      {count > 0 && (
        <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
          {count}
        </span>
      )}
    </Link>
  );
};

export default LikedButton;
