import Link from "next/link";
import Navbar from "./navbar/navbar.jsx";
import React, { useState } from "react";
import CartButton from "./cart-button.jsx";
import NavbarMobile from "./navbar/mobile-navbar.jsx";
import LikedButton from "./liked-buttin.jsx";
import LangSwitcher from "./lang-switcher.jsx";
import Notification from "../ui/notification.jsx";
import { useNotificationCtx } from "@/store/notification-context.jsx";
import Image from "next/image.js";
import { GiHamburgerMenu } from "react-icons/gi";

const MainHeader = () => {
  const { notification } = useNotificationCtx();
  const [showMenu, setShowMenu] = useState(false);

  function menuToggle() {
    setShowMenu(!showMenu);
  }

  return (
    <header className="fixed-top header py-2">
      <div className="position-relative container d-flex align-items-center justify-content-between">
        <Link href="/" aria-label="Go to home page">
          <Image 
            src='/images/logo-bg.png'
            alt='logo'
            width={60}
            height={60}
            className="rounded-pill"
          />
        </Link>

        <Navbar />
        <div className="d-flex">
          <div className="d-flex align-items-center">
            {/* <LikedButton /> */}
            <CartButton />
            <LangSwitcher />
          </div>

          <button
            onClick={menuToggle}
            className="btn btn-outline-warning border-0 d-lg-none"
          >
            <GiHamburgerMenu className="fs-5" />
          </button>

          <NavbarMobile showMenu={showMenu} toggleMenu={menuToggle} />
        </div>

        {notification && (
          <Notification
            title={notification.title}
            message={notification.message}
            status={notification.status}
          />
        )}
      </div>
    </header>
  );
};

export default MainHeader;
