import React, { useState, useEffect, useRef } from "react";
import Image from "next/image";
import { useRouter } from "next/router";

const LangSwitcher = () => {
  const dropdownRef = useRef(null);
  const [show, setShow] = useState(false);
  const { locale, push, pathname, query } = useRouter();

  const changeLang = (lang) => {
    push({pathname, query}, undefined, { locale: lang });

    setShow(false);
  };

  const toggleShow = () => {
    setShow(!show);
  };

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setShow(false);
      }
    };
    window.addEventListener("click", handleOutsideClick);

    return () => {
      window.removeEventListener("click", handleOutsideClick);
    };
  }, [dropdownRef]);

  return (
    <div className="dropdown-center" ref={dropdownRef}>
      <button
        className="btn btn-sm border-0 text-white dropdown-toggle border-top-0"
        id="langSwitcher"
        data-toggle="dropdown"
        onClick={toggleShow}
      >
        <Image
          src={`/images/${locale}.svg`}
          width="16"
          height="12"
          className="me-1"
          alt="am"
        />
      </button>

      <div
        className={`dropdown-menu p-1 end-0 mt-1 ${show && "show"}`}
        aria-labelledby="langSwitcher"
      >
        <button
          className="dropdown-item rounded"
          onClick={changeLang.bind(null, "am")}
        >
          <span>
            <Image
              src="/images/am.svg"
              width="16"
              height="12"
              className="me-2"
              alt="am"
            />
            Հայերեն
          </span>
        </button>
        <button
          className="dropdown-item rounded"
          onClick={changeLang.bind(null, "ru")}
        >
          <span>
            <Image
              src="/images/ru.svg"
              width="16"
              height="12"
              className="me-2"
              alt="ru"
            />
            Русский
          </span>
        </button>
        <button
          className="dropdown-item rounded"
          onClick={changeLang.bind(null, "en")}
        >
          <span>
            <Image
              src="/images/en.svg"
              width="16"
              height="12"
              className="me-2"
              alt="en"
            />
            English
          </span>
        </button>
      </div>
    </div>
  );
};

export default LangSwitcher;
