import React from "react";
import { useNotificationCtx } from "@/store/notification-context.jsx";

function Notification({ title, message, status }) {
  const { notification, hideNotification } = useNotificationCtx();

  let styles;
  if (status === "success") {
    styles = "text-green";
  }

  if (status === "error") {
    styles = "text-danger";
  }

  if (status === "pending") {
    styles = "text-warning";
    message = (
      <div className="d-flex justify-content-center">
        <div className="spinner-border text-warning" role="status"></div>
      </div>
    );
  }

  return (
    <div className={`toast notification ${notification && "show"}`}>
      <div className="toast-header">
        <strong className={`me-auto ${styles}`}>{title}</strong>
        <button
          type="button"
          className="btn-close"
          onClick={hideNotification}
        ></button>
      </div>
      <div className="toast-body">{message}</div>
    </div>
  );
}

export default Notification;
