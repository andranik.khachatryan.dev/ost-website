import React, { useRef, useState } from "react";
import { useNotificationCtx } from "@/store/notification-context";
import axios from "axios";

const CategoryesAdmin = () => {
  const { showNotification } = useNotificationCtx();
  const findInput = useRef(null);
  const [searchItem, setSearchItem] = useState(null);

  const amName = useRef(null);
  const enName = useRef(null);
  const ruName = useRef(null);
  const priorityInp = useRef(null);

  const submitEditHandler = (e) => {
    e.preventDefault();
    console.log(e.target.id, "e");
  };

  const submitHandler = (e) => {
    e.preventDefault();

    showNotification({
      title: "Creating Category",
      message: "Your category saving in to the database",
      status: "pending",
    });

    const am = amName.current.value;
    const en = enName.current.value;
    const ru = ruName.current.value;
    const priority = priorityInp.current.value;

    if (
      am.trim === "" ||
      en.trim() === "" ||
      ru.trim() === "" ||
      priority <= 0
    ) {
      //setnotifiction fill all fields
      return;
    } else {
      const data = {
        am,
        en,
        ru,
        priority,
      };
      const createCategory = async (data) => {
        const res = await axios.post(
          "http://localhost:3001/categoryes/create",
          data
        );
        return res.data;
      };

      createCategory(data)
        .then((newItem) => {
          showNotification({
            title: "Success!",
            message: "The new category was created and saved!",
            status: "success",
          });
        })
        .catch((e) => {
          showNotification({
            title: "Error!",
            message: e.message || "Something went wrong",
            status: "error",
          });
        });
      amName.current.value = "";
      enName.current.value = "";
      ruName.current.value = "";
      priorityInp.current.value = "";
    }
  };

  const submitFindHandler = (e) => {
    e.preventDefault();
    const findVal = findInput.current.value;
    findCategory(findVal)
      .then((item) => {
        console.log("exav");
        setSearchItem(item);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deleteHandler = (id) => {
    showNotification({
      title: "Deleting Category",
      message: "Your category deleting from the database",
      status: "pending",
    });
    deleteCategory(id)
      .then((deletedItem) => {
        showNotification({
          title: "Success!",
          message: "The Category was deleted !",
          status: "success",
        });
        setSearchItem(null);
      })
      .catch((error) => {
        showNotification({
          title: "Error!",
          message: error.message || "Something went wrong",
          status: "error",
        });
      });
  };

  const findCategory = async (name) => {
    const response = await axios.get(
      `http://localhost:3001/categoryes/search?name=${name}`
    );
    return response.data[0];
  };

  const deleteCategory = async (id) => {
    const res = await axios.delete(`http://localhost:3001/categoryes/${id}`);
    return res.data;
  };

  return (
    <div className="border border-2 border-warning rounded-2 p-2">
      <div className="row">
        <h4 className="text-warning">Categoryes</h4>
        <div className="col-12 col-lg-6">
          <p className="text-center">Edit Categoryes</p>
          <form className="input-group mb-3" onSubmit={submitFindHandler}>
            <input
              type="text"
              className="form-control"
              placeholder="Find by Name"
              ref={findInput}
            />
            <button className="btn btn-warning" type="submit">
              Button
            </button>
          </form>
          {searchItem && (
            <div className="p-2 border border-warning rounded">
              <div className="d-flex align-items-center justify-content-between">
                <div>
                  {searchItem._id}
                  <span className="me-3">
                    <strong className="me-1">AM:</strong>
                    {searchItem.am}
                  </span>
                  <span className="me-3">
                    <strong className="me-1">EN:</strong>
                    {searchItem.en}
                  </span>
                  <span className="me-3">
                    <strong className="me-1">RU:</strong>
                    {searchItem.ru}
                  </span>
                </div>
                <div>
                  <button
                    className="btn btn-sm btn-close"
                    onClick={() => setSearchItem(null)}
                  ></button>
                </div>
              </div>

              <div className="row mt-3">
                <div className="col-12 col-sm-4">
                  <form
                    className="input-group input-group-sm mb-3"
                    onSubmit={submitEditHandler}
                    id="editCategoryAm"
                  >
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Armenian name"
                    />
                    <button
                      className="btn btn-sm btn-warning"
                      type="submit"
                      id="button-addon2"
                    >
                      Save
                    </button>
                  </form>
                </div>
                <div className="col-12 col-sm-4">
                  <form
                    className="input-group input-group-sm mb-3"
                    onSubmit={submitEditHandler}
                    id="editCategoryEn"
                  >
                    <input
                      type="text"
                      className="form-control"
                      placeholder="English name"
                    />
                    <button className="btn btn-sm btn-warning" type="submit">
                      Save
                    </button>
                  </form>
                </div>
                <div className="col-12 col-sm-4">
                  <form
                    className="input-group input-group-sm mb-3"
                    onSubmit={submitEditHandler}
                    id="editCategoryRu"
                  >
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Russian name"
                    />
                    <button className="btn btn-sm btn-warning" type="submit">
                      Save
                    </button>
                  </form>
                </div>
              </div>
              <div className="text-center">
                <button
                  className="btn btn-sm btn-danger"
                  onClick={deleteHandler.bind(null, searchItem._id)}
                >
                  Delete
                </button>
              </div>
            </div>
          )}
        </div>
        <div className="col-12 col-lg-6">
          <p className="text-center">Create new Category</p>
          <form onSubmit={submitHandler}>
            <div className="row mb-2">
              <div className="col col-lg-6">
                <div className="input-group flex-nowrap mb-2">
                  <span className="input-group-text">am</span>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="armenian name"
                    ref={amName}
                  />
                </div>
              </div>
              <div className="col col-lg-6">
                <div className="input-group flex-nowrap mb-2">
                  <span className="input-group-text">en</span>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="english name"
                    ref={enName}
                  />
                </div>
              </div>
            </div>
            <div className="row mb-2">
              <div className="col col-lg-6">
                <div className="input-group flex-nowrap mb-2">
                  <span className="input-group-text">ru</span>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="russian name"
                    ref={ruName}
                  />
                </div>
              </div>
              <div className="col col-lg-6">
                <div className="input-group flex-nowrap mb-2">
                  <span className="input-group-text">priority</span>
                  <input
                    type="number"
                    className="form-control"
                    placeholder="priority number"
                    ref={priorityInp}
                  />
                </div>
              </div>
            </div>
            <div className="d-flex">
              <button className="btn btn-warning my-3 mx-auto" type="submit">
                Create Category
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CategoryesAdmin;
