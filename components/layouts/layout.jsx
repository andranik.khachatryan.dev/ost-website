import React from "react";
import MainFooter from "../sections/footer/main-footer.jsx";
import MainHeader from "../header/main-header.jsx";

const Layout = (props) => {
  return (
    <div className="bg-secondary position-relative">
      <MainHeader />
      <main className="scroll-smooth">{props.children}</main>
      <MainFooter />
    </div>
  );
};

export default Layout;
