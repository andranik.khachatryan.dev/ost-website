import Image from "next/image";

function HeroSection({ texts }) {
  return (
    <section className="position-relative vh-100" id="hero">
      <Image
        src="/images/ost-slide-1.png"
        alt="bg-img"
        fill
        style={{ objectFit: "cover" }}
      />
      <div className="overlay position-absolute start-0 top-0 w-100 h-100 d-flex justify-content-center align-items-center">
        <div className="container">
          <div className="row">
            <div className="col-lg-8 mx-auto d-flex justify-content-center align-items-center">
              <div className="text-center">
                <h2 className="text-warning animate__animated animate__fadeInDown">
                  {texts.heading}
                </h2>
                <p className="text-white animate__animated animate__fadeInUp">
                  {texts.subHeading}
                </p>
                <div className="mx-auto">
                  <a
                    href="#menu"
                    className="btn btn-warning border-2 rounded-pill mx-2 mb-2"
                  >
                    {texts.menuBtn}
                  </a>
                  <a
                    href="#book-a-table"
                    className="btn btn-warning border-2 rounded-pill mx-2 mb-2"
                  >
                    {texts.contactBtn}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default HeroSection;
