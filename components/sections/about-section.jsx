import Image from "next/image";

const AboutSection = ({ texts }) => {
  return (
    <section className="py-5 logo-bg" id="about">
      <div className="container py-4">
        <div className="row mb-5">
          <div className="col-xl-6 d-flex flex-column justify-content-center align-items-stretch">
            <h2>{texts.heading}</h2>
            <p>{texts.p1}</p>
            <p>{texts.p2}</p>
          </div>
          <div className="col-xl-6 d-flex flex-column justify-content-center align-items-stretch">
            <Image
              src="/images/about-sec.png"
              alt="img"
              width="1360"
              height="1020"
              style={{ width: "100%", height: "auto" }}
              className="rounded-4"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-xl-6 d-flex justify-content-center align-items-center">
            <Image
              src="/images/about-sec-wine.png"
              alt="img"
              width="1360"
              height="1020"
              style={{ width: "100%", height: "auto" }}
              className="rounded-4"
            />
          </div>
          <div className="col-xl-6 d-flex justify-content-center align-items-center">
            <div>
              <p>{texts.p3}</p>
              <p>{texts.p4}</p>
              <p>{texts.p5}</p>
              <button className="btn border-2 btn-outline-warning rounded-pill">
                {texts.checkWinesBtn}
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutSection;
