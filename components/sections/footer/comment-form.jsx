import axios from "axios";
import React, { useRef } from "react";
import { useNotificationCtx } from "@/store/notification-context.jsx";

const CommentForm = ({ btnText, inputPlaceholder }) => {
  const { showNotification } = useNotificationCtx();
  const commentText = useRef(null);

  const sendMessage = async (event) => {
    event.preventDefault();
    showNotification({
      title: "Sending comment",
      message: "your comment saving in to the database",
      status: "pending",
    });
    const message = commentText.current.value;

    try {
      await axios.post("/api/comments", { comment: message });

      showNotification({
        title: "Success!",
        message: "Your comment was sent!",
        status: "success",
      });
    } catch (error) {
        
      showNotification({
        title: "Error!",
        message: error.message || "something went wrong",
        status: "error",
      });
    }
    commentText.current.value = "";
  };

  return (
    <form className="input-group my-2" onSubmit={sendMessage}>
      <input
        ref={commentText}
        type="text"
        className="form-control"
        placeholder={inputPlaceholder}
        aria-label="Recipient's username"
      ></input>
      <button className="btn btn-warning" type="submit">
        {btnText}
      </button>
    </form>
  );
};

export default CommentForm;
