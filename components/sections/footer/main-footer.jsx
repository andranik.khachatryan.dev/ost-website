import React, { useRef } from "react";
import { useTranslation } from "next-i18next";
import { FaFacebook, FaViber, FaMapMarkerAlt } from "react-icons/fa";
import { SlSocialInstagram } from "react-icons/sl";
import { IoCall } from "react-icons/io5";
import CommentForm from "./comment-form";
import Link from "next/link";

const MainFooter = () => {
  const { t } = useTranslation("footer");

  return (
    <>
      <footer className="bg-brown">
        <div className="container py-5">
          <div className="row">
            <div className="col d-flex justify-content-center text-center align-items-center mx-auto">
              <div>
                <h6 className="fs-3 text-warning">{t("feedback-title")}</h6>
                <p className="text-light">{t("feedback-text")}</p>
                <CommentForm
                  btnText={t("feedback-btn")}
                  inputPlaceholder={t("feedback-input-placeholder")}
                />
                <div className="text-light my-3">
                  <Link target="_blank" href="https://www.google.com/maps/place/%D5%95%D5%BD%D5%BF+%D5%A3%D5%AB%D5%B6%D5%AB+%D6%87+%D5%B8%D6%82%D5%BF%D5%A5%D5%AC%D5%AB%D6%84/@40.1863517,44.5067303,19z/data=!4m6!3m5!1s0x406abd1cd8698cd1:0x9db6587a27e7704b!8m2!3d40.1860145!4d44.5074907!16s%2Fg%2F11c7wf0zb0" className="me-3">
                    <span className="me-1">
                      <FaMapMarkerAlt />
                    </span>
                    {t("address")}
                  </Link>
                  <Link href="tel:+37410234334">
                    <span className="me-1">
                      <IoCall />
                    </span>
                    010 234 334
                  </Link>
                </div>
                <div>
                  <Link href="https://www.facebook.com/OstFoodsAndWine/" target="_blank" className="me-3">
                    <FaFacebook className="fs-1" />
                  </Link>
                  <Link href="https://www.instagram.com/ost.restaurant/" target="_blank" className="me-3">
                    <SlSocialInstagram className="fs-1" />
                  </Link>
                  <Link href="#">
                    <FaViber className="fs-1" />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid overlay px-0 py-3">
          <div className="text-secondary d-flex justify-content-center">
            <span className="fs-smallest text-center">© {t("copyright")}</span>
          </div>
        </div>
      </footer>
    </>
  );
};

export default MainFooter;
