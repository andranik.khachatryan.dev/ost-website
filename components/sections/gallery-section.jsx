import React from "react";
import Image from "next/image";

const imgNames = [
  "foods.png",
  "sexan.jpg",
  "kenac.jpg",
  "aperol.jpg",
  "sexanner.jpg",
  "coffe.jpg",
  "hayat.jpg",
  "kenac-2.jpg",
];
const GallerySection = ({ texts }) => {
  return (
    <section id="gallery" className="gallery">
      <div className="container py-5">
        <div className="section-title">
          <h2>{texts.heading}</h2>
          <p>{texts.subHeading}</p>
        </div>
        <div className="row g-0">
          {imgNames.map((name) => (
            <div className="col-lg-3 col-6" key={name}>
              <div className="gallery-item position-relative card-img-wraper">
                <Image
                  src={`/images/${name}`}
                  alt={name}
                  fill
                  sizes="100%"
                  style={{ objectFit: "cover" }}
                />
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default GallerySection;
