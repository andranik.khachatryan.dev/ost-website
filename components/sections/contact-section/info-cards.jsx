import React from "react";
import { IoLocationSharp, IoTimeSharp, IoCall } from "react-icons/io5";
import { HiMail } from "react-icons/hi";
import Link from "next/link";

const InfoCards = ({ callText, emailText, openDays, openHours, address, addressRegion, location }) => {
  return (
    <div className="info-wrap">
      <div className="row">
        <div className="col-lg-3 col-md-6 info">
          <Link
            target="_blank"
            href="https://www.google.com/maps/place/%D5%95%D5%BD%D5%BF+%D5%A3%D5%AB%D5%B6%D5%AB+%D6%87+%D5%B8%D6%82%D5%BF%D5%A5%D5%AC%D5%AB%D6%84/@40.1863517,44.5067303,19z/data=!4m6!3m5!1s0x406abd1cd8698cd1:0x9db6587a27e7704b!8m2!3d40.1860145!4d44.5074907!16s%2Fg%2F11c7wf0zb0"
            className="icon"
          >
            <IoLocationSharp className="fs-3" />
          </Link>
          <h4>{location}</h4>
          <p>
            {address}
            <br />
            {addressRegion}
          </p>
        </div>

        <div className="col-lg-3 col-md-6 info mt-4 mt-lg-0">
          <Link href="tel:+37410234334" className="icon">
            <IoTimeSharp className="fs-3" />
          </Link>
          <h4>{openHours}</h4>
          <p>
            {openDays}
            <br />
            11:00 AM - 23:00 PM
          </p>
        </div>

        <div className="col-lg-3 col-md-6 info mt-4 mt-lg-0">
          <Link href="mailto:ostfoodsandwine@gmail.com" className="icon">
            <HiMail className="fs-3" />
          </Link>
          <h4>{emailText}</h4>
          <p>ostfoodsandwine@gmail.com</p>
        </div>

        <div className="col-lg-3 col-md-6 info mt-4 mt-lg-0">
          <Link href="tel:+37410234334" className="icon">
            <IoCall className="fs-3" />
          </Link>
          <h4>{callText}</h4>
          <p>
            (010) 234 334
            <br />
            (010) 600 808
          </p>
        </div>
      </div>
    </div>
  );
};

export default InfoCards;
