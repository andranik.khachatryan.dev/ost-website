import React, { useRef } from "react";
import { useTranslation } from "next-i18next";
import InfoCards from "./info-cards.jsx";
import { useNotificationCtx } from "@/store/notification-context.jsx";
import axios from "axios";

const ContactSection = () => {
  const { t } = useTranslation("contact");
  const { showNotification } = useNotificationCtx();

  const texts = {
    callText: t("call-text"),
    emailText: t("email-text"),
    openDays: t("open-days-text"),
    openHours: t("open-hours-text"),
    address: t("address-text"),
    addressRegion: t("address-text-region"),
    location: t("location-text"),
  };

  const nameRef = useRef(null);
  const phoneRef = useRef(null);
  const titleRef = useRef(null);
  const messageRef = useRef(null);

  const submitHandler = async (e) => {
    e.preventDefault();
    showNotification({
      title: "Sending Message",
      message: "your message saving in to the database",
      status: "pending",
    });

    const name = nameRef.current.value;
    const phone = phoneRef.current.value;
    const title = titleRef.current.value;
    const message = messageRef.current.value;

    try {
      await axios.post("/api/contact", {
        name,
        phone,
        title,
        message,
      });

      showNotification({
        title: "Success!",
        message: "Your Message was sent!",
        status: "success",
      });
    } catch (error) {
      showNotification({
        title: "Error!",
        message: error.message || "something went wrong",
        status: "error",
      });
    }
    nameRef.current.value = "";
    phoneRef.current.value = "";
    titleRef.current.value = "";
    messageRef.current.value = "";
  };

  return (
    <section id="contact" className="contact py-5">
      <div className="container">
        <div className="section-title">
          <h2>{t("contact-heading")}</h2>
          <p>{t("contact-subheading")}</p>
        </div>
        <div className="map">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3048.0357171270502!2d44.504915775401756!3d40.18601856964989!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x406abd1cd8698cd1%3A0x9db6587a27e7704b!2z1ZXVvdW_INWj1avVttWrINaHINW41oLVv9Wl1azVq9aE!5e0!3m2!1shy!2sam!4v1684848677228!5m2!1shy!2sam"
            style={{ border: "0", width: "100%", height: "350px" }}
            allowFullScreen={true}
            loading="lazy"
            referrerPolicy="no-referrer-when-downgrade"
          ></iframe>
        </div>
      </div>

      <div className="container mt-5">
        <InfoCards {...texts} />

        <form onSubmit={submitHandler} className="php-email-form">
          <div className="row">
            <div className="col-md-6 form-group">
              <input
                type="text"
                name="name"
                className="form-control"
                id="name"
                placeholder={t("name-placeholder")}
                ref={nameRef}
              ></input>
            </div>
            <div className="col-md-6 form-group mt-3 mt-md-0">
              <input
                type="text"
                className="form-control"
                name="email"
                id="email"
                placeholder={t("email-placeholder")}
                ref={phoneRef}
              ></input>
            </div>
          </div>
          <div className="form-group mt-3">
            <input
              type="text"
              className="form-control"
              name="subject"
              id="subject"
              placeholder={t("subject-placeholder")}
              ref={titleRef}
            ></input>
          </div>
          <div className="form-group mt-3">
            <textarea
              className="form-control"
              name="message"
              rows="5"
              placeholder={t("message-placeholder")}
              ref={messageRef}
            ></textarea>
          </div>
          <div className="text-center mt-2">
            <button type="submit">{t("send-message-btn")}</button>
          </div>
        </form>
      </div>
    </section>
  );
};

export default ContactSection;
