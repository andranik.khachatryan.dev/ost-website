import React, { useEffect, useState } from "react";
import { useLikedContext } from "@/store/liked-context";
import { AiOutlineHeart, AiFillHeart } from "react-icons/ai";
import { RiShoppingCartLine } from 'react-icons/ri'
import { useTranslation } from "next-i18next";
import Link from "next/link";

const CardButtons = (props) => {
  const { t } = useTranslation("product-card");
  const { items } = useLikedContext();
  const [count, setCount] = useState(1);
  const [isLiked, setIsLiked] = useState(null);

  useEffect(() => {
    const bool = items.some((item) => item.id === props.id);
    setIsLiked(bool);
  }, [items]);

  const addCount = () => {
    setCount(count + 1);
  };
  const removeCount = () => {
    const validCount = count - 1 >= 1;
    if (!validCount) {
      return;
    }
    setCount(count - 1);
  };
  const addToCart = () => {
    if (count >= 1) {
      props.onAddToCart(count);
    }
  };
  const toggleLiked = () => {
    props.onToggleLiked();
  };
  return (
    <>
      <div className="row g-2 mb-2">
        <div className="col d-flex align-items-center">
          <div
            className="btn-group w-100"
            role="group"
            aria-label="Basic example"
          >
            <button
              className="btn btn-outline-warning btn-sm"
              onClick={removeCount}
            >
              -
            </button>
            <span className="px-3 d-flex align-items-center bg-secondary">
              {count}
            </span>
            <button
              className="btn btn-outline-warning btn-sm"
              onClick={addCount}
            >
              +
            </button>
          </div>
        </div>
        <div className="col d-flex align-items-center">
          <Link
            href={`/product/${props.name}`}
            className="btn btn-sm btn-outline-primary w-100 me-2"
          >
            {t("details-btn")}
          </Link>
          <button
            className={`btn btn-sm border-0 text-danger p-1`}
            onClick={toggleLiked}
          >
            {isLiked ? (
              <AiFillHeart className="fs-3" />
            ) : (
              <AiOutlineHeart className="fs-3" />
            )}
          </button>
        </div>
      </div>
      <button className="btn btn-warning w-100" onClick={addToCart}>
        {t("add-to-cart-btn")}
        <RiShoppingCartLine  className="ms-1"/>
      </button>
    </>
  );
};

export default CardButtons;
