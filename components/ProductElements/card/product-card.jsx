import React, { useContext } from "react";
import CardButtons from "./card-buttons.jsx";
import CartContext from "@/store/cart-context.jsx";
import { useLikedContext } from "@/store/liked-context.jsx";
import { useRouter } from "next/router";
import Image from "next/image.js";
import Link from "next/link.js";

const ProductCard = ({ name, price, imgSrc, id }) => {
  const cartCtx = useContext(CartContext);
  const { toggleItem } = useLikedContext();
  const { locale } = useRouter();

  // const isCardLiked = isLiked(id);

  const addToCartHandler = (amount) => {
    cartCtx.addItem({
      id: id,
      name: name,
      amount: amount,
      price: price,
      imgSrc: imgSrc,
    });
  };
  const toggleLikedHandler = () => {
    toggleItem({
      id,
      name,
      price,
      imgSrc,
    });
  };

  // change this , set Link to not use code below and inside the link set Image
  const router = useRouter();
  const clickOnCard = () => {
    // const convertedStr = name['en'].replace(/\s+/g, '-');
    const convertedStr = name[locale];
    if (name[locale]) {
      router.push(`/product/${convertedStr}`);
    }
  };

  return (
    <div className="card h-100 overflow-hidden">
      <Link
        href={`/product/${name[locale]}`}
        className="pointer position-relative card-img-wraper"
      
      >
        <Image
          src={imgSrc}
          fill
          sizes="100%"
          alt="product photo"
          style={{ objectFit: "cover"}}
        />
      </Link>
      <div className="card-body d-flex flex-column justify-content-between p-2">
        <div className="mb-2 text-center">
          <div className="mb-0">{name[locale]}</div>
          <div className="mb-0">{price} AMD</div>
        </div>
        <CardButtons
          onAddToCart={addToCartHandler}
          onToggleLiked={toggleLikedHandler}
          id={id}
          name={name}
        />
      </div>
    </div>
  );
};

export default ProductCard;
