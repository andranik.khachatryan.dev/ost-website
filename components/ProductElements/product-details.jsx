import React, { useContext, useState, useEffect } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useLikedContext } from "@/store/liked-context";
import { AiOutlineHeart, AiFillHeart } from "react-icons/ai";
import CartContext from "@/store/cart-context";

const ProductDetails = ({ product }) => {
  const { locale } = useRouter();
  const cartCtx = useContext(CartContext);
  const { toggleItem, items } = useLikedContext();
  const [count, setCount] = useState(1);
  const [isLiked, setIsLiked] = useState(null);

  const addToCartHandler = () => {
    cartCtx.addItem({
      id: product._id,
      name: product.name,
      amount: count,
      price: product.price,
      imgSrc: product.imageSrc,
    });
  };
  const toggleLikedHandler = () => {
    toggleItem({
      id: product._id,
      name: product.name,
      price: product.price,
      imgSrc: product.imageSrc,
    });
  };
  const addCount = () => {
    setCount(count + 1);
  };
  const removeCount = () => {
    const validCount = count - 1 >= 1;
    if (!validCount) {
      return;
    }
    setCount(count - 1);
  };

  useEffect(() => {
    const bool = items.some((item) => item.id === product._id);
    setIsLiked(bool);
  }, [items]);

  return (
    <div className="row align-items-center">
      <div className="col-md-6 mb-3 mb-md-0">
        <Image
          width="600"
          height="400"
          className="img-fluid"
          priority
          src={product.imageSrc}
          alt={product.name[locale]}
        />
      </div>
      <div className="col-md-6">
        <div className="row">
          <div className="col-md-10">
            <h4 className="display-5 fw-bolder">{product.name[locale]}</h4>
            <div className="fs-5 mb-4">
              <span>{product.price} AMD </span>
            </div>
            <p className="lead">{product.description[locale]}</p>
            <div className="col">
              <div className=" d-flex align-items-center pb-3 border-2 border-bottom border-warning">
                <div
                  className="btn-group me-2"
                  role="group"
                  aria-label="Basic example"
                >
                  <button
                    className="btn btn-outline-warning"
                    onClick={removeCount}
                  >
                    -
                  </button>
                  <span className="px-3 d-flex align-items-center bg-secondary">
                    {count}
                  </span>
                  <button
                    className="btn btn-outline-warning"
                    onClick={addCount}
                  >
                    +
                  </button>
                </div>
                <button className="btn btn-warning" onClick={addToCartHandler}>
                  Add to Cart
                </button>
              </div>
              <div className=" d-flex align-items-center">
                <p className="mb-0"> Add to wishlist</p>

                <button
                  className={`btn border-0 text-danger p-1 ms-2`}
                  onClick={toggleLikedHandler}
                >
                  {isLiked ? (
                    <AiFillHeart className="fs-2" />
                  ) : (
                    <AiOutlineHeart className="fs-2" />
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetails;
