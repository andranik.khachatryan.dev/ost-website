import React, { useState } from "react";
import ProductCard from "../card/product-card.jsx";
import Link from "next/link";
import ProductSorting from "./sorting-dropdown.jsx";
import SearchInput from "./search-input.jsx";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router.js";

const ProductList = ({ items, category }) => {
  const { locale } = useRouter()
  const { t } = useTranslation("filter");
  const sortingTexts = {
    sortBy: t("sort-by"),
    byPopularity: t("by-popularity"),
    byHighest: t("by-price-highest"),
    byLowest: t("by-price-lowest"),
  };
  const [sortBy, setSortBy] = useState("default");
  const [searchText, setSearchText] = useState("");

  const setSortByHandler = (sort) => {
    setSortBy(sort);
  };

  const sortedProducts = items.sort((a, b) => a.priority - b.priority);
  if (sortBy === "rate-up") {
    sortedProducts.sort((a, b) => b.price - a.price);
  } else if (sortBy === "rate-down") {
    sortedProducts.sort((a, b) => a.price - b.price);
  }

  // const filteredProducts = sortedProducts.filter((product) =>
  //   product.name[locale].toLowerCase().startsWith(searchText.toLowerCase())
  // );
  const filteredProducts = sortedProducts.filter((product) => {
    const searchLowerCase = searchText.toLowerCase();
    const locales = ['am', 'en', 'ru']
    for (const locale of locales) {
      const localeName = product.name[locale].toLowerCase();
      if (localeName.startsWith(searchLowerCase)) {
        return true; // Include the product if any locale name matches the search text
      }
    }
    
    return false; // Exclude the product if none of the locale names match the search text
  });

  return (
    <>
      <div className="row gx-3 pb-2 mb-3 border-bottom border-2 border-warning">
        <div className="col-sm-7 col-lg-8 d-flex align-items-center justify-content-between mb-2 mb-sm-0">
          {category ? (
            <Link href="/products-category" className="btn btn-sm btn-warning">
              {t("all-products")}
            </Link>
          ) : (
            <span>{t("all-products")}</span>
          )}
          <ProductSorting onSetSortBy={setSortByHandler} texts={sortingTexts} />
        </div>
        <div className="col-sm-5 col-lg-4 text-end">
          <SearchInput
            searchText={searchText}
            setSearchText={setSearchText}
            text={t("search-input")}
          />
        </div>
      </div>
      <div className="row g-2 g-md-3">
        {filteredProducts.length === 0 && <div className="col-12 d-flex">
            <div className="mx-auto pt-5 mt-5 text-center">
                <p>{t('no-founded-product')}</p>
            </div>
          </div>}
        {filteredProducts.length !== 0 && filteredProducts.map((i) => (
          <div className="col-6 col-md-4 col-xl-3" key={i._id}>
            <ProductCard
              name={i.name}
              price={i.price}
              imgSrc={i.imageSrc}
              id={i._id}
            />
          </div>
        ))}
      </div>
    </>
  );
};

export default ProductList;
