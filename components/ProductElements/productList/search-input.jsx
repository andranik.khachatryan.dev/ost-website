import React from "react";
import { BiSearch } from 'react-icons/bi'
const SearchInput = ({ searchText, setSearchText, text }) => {
 

  return (
    <div className="input-group input-group-sm">
      <input
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
        type="text"
        className="form-control"
        placeholder={text}
      />
      <span className="input-group-text bg-warning" id="basic-addon2">
        <BiSearch className="fs-5"/>
      </span>
    </div>
  );
};

export default SearchInput;
