import React, { useEffect, useRef, useState } from "react";

const ProductSorting = ({ onSetSortBy, texts }) => {
  const dropdownRef = useRef(null);
  const [showSorting, setShowSorting] = useState(false);

  const setSortByHandler = (sort) => {
    onSetSortBy(sort);
    setShowSorting(false);
  };

  const toggleShowSorting = () => {
    setShowSorting(!showSorting);
  };

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setShowSorting(false);
      }
    };
    window.addEventListener("click", handleOutsideClick);

    return () => {
      window.removeEventListener("click", handleOutsideClick);
    };
  }, [dropdownRef]);

  return (
    <div className="dropdown-center" ref={dropdownRef}>
      <button
        className="btn btn-sm  btn-warning dropdown-toggle border-top-0"
        id="sortingOptions"
        data-toggle="dropdown"
        onClick={toggleShowSorting}
      >
        {texts.sortBy}
      </button>
      <div
        className={`dropdown-menu p-1 end-0 mt-1 ${showSorting && "show"}`}
        aria-labelledby="sortingOptions"
      >
        <button
          className="dropdown-item rounded"
          onClick={setSortByHandler.bind(null, "default")}
        >
          {texts.byPopularity}
        </button>
        <button
          className="dropdown-item rounded"
          onClick={setSortByHandler.bind(null, "rate-up")}
        >
          {texts.byHighest}
        </button>
        <button
          className="dropdown-item rounded"
          onClick={setSortByHandler.bind(null, "rate-down")}
        >
          {texts.byLowest}
        </button>
      </div>
    </div>
  );
};

export default ProductSorting;
