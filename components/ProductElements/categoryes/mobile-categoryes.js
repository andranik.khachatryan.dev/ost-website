import Categoryes from "./categoryes";
import { useState } from "react";

const CategoryesMobile = ({categoryes}) => {
  const [show, setShow] = useState(false);
  const downSvg = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      fill="currentColor"
      className="bi bi-chevron-down"
      viewBox="0 0 16 16"
    >
      <path
        fillRule="evenodd"
        d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
      />
    </svg>
  );

  return (
    <div className="row d-lg-none">
      <div className="col-12 col-sm-8 mx-auto">
        <button
          className={`btn btn-sm btn-warning colapse-btn border-2 w-100 mb-2 ${show && 'active'}`}
          onClick={() => setShow(!show)}
        >
          Categoryes {downSvg}
        </button>
        <div className={`collapse ${show ? "show" : ""}`} id="collapseExample">
          <div className="mb-4">
            <Categoryes categoryes={categoryes}/>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CategoryesMobile;
