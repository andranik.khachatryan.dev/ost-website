import Link from "next/link";
import { useRouter } from "next/router";
import { BiChevronRight } from "react-icons/bi";

function Categoryes({ selectedCategory, categoryes }) {
  const { locale } = useRouter();

  // const sortedCategories = categoryes.sort((a, b) => a.priority - b.priority);
  return (
    <ul className="nav flex-column">
      {categoryes &&
        categoryes.map((category) => (
          <li className="rounded-4 mb-1" key={category._id}>
            <Link
              href={`/products-category/${category[locale]}`}
              className={`category-link rounded-2 p-1 ${
                selectedCategory === category[locale] ? "active" : ""
              }`}
            >
              <BiChevronRight className="fs-5" />
              {category[locale]}
            </Link>
          </li>
        ))}
    </ul>
  );
}

export default Categoryes;
