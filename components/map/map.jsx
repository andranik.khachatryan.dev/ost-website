import React, { useState, useEffect } from "react";
import { MapContainer, TileLayer, useMap, Marker, Popup, useMapEvents } from "react-leaflet";
import { Icon } from "leaflet";
import "leaflet/dist/leaflet.css";
import "leaflet-defaulticon-compatibility";
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css'

const Map = () => {
  const [userLocation, setUserLocation] = useState(null);
  const [address, setAddress] = useState(null);

  useEffect(() => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(async (position) => {
        const { latitude, longitude } = position.coords;
        setUserLocation({ lat: latitude, lng: longitude });

        try {
          const response = await fetch(`https://nominatim.openstreetmap.org/reverse?lat=${latitude}&lon=${longitude}&format=json`, {
            headers: {
              "Accept-Language": "en-US,en;q=0.9" // Set the desired language here
            }
          });
          const data = await response.json();
          setAddress(data.display_name);
        } catch (error) {
          setAddress('Error retrieving address');
          console.error(error);
        }
      });
    }
  }, []);

  const defaultIcon = new Icon.Default();

  // no working
  const handleClick = async (e) => {
    const { lat, lng } = e.latlng;

    try {
      const response = await fetch(`https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lng}&format=json`, {
        headers: {
          "Accept-Language": "en-US,en;q=0.9" // Set the desired language here
        }
      });
      const data = await response.json();
      setAddress(data.display_name);
    } catch (error) {
      setAddress('Error retrieving address');
      console.error(error);
    }
  };
  

  return (
    <div>
      {userLocation ? (
        <div>
          <MapContainer
            center={userLocation}
            zoom={20}
            scrollWheelZoom={false}
            style={{ height: "400px", width: "100%" }}
            onClick={handleClick}
          >
            <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
            <Marker position={userLocation} icon={defaultIcon}>
              <Popup>{address}</Popup>
            </Marker>
          </MapContainer>
          <p>Current address: {address}</p>
        </div>
      ) : (
        <p>Loading map...</p>
      )}
    </div>
  );
};

export default Map;
