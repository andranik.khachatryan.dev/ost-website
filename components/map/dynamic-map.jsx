import dynamic from "next/dynamic";

const DynamicMap = dynamic(() => import('./map.jsx'), {
    ssr: false
})

export default DynamicMap;