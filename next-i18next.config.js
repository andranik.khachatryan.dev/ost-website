module.exports = {
  i18n: {
    defaultLocale: "am",
    locales: ["am", "en", "ru"],
    localeDetection: false,
  },
};
