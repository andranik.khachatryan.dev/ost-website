import axios from "axios";
const domainName = "http://localhost:3000";

// products api util
export async function getAllProducts() {
  const res = await axios.get(`${domainName}/api/products`);
  const products = await res.data.data;
  return products;
}

export async function getProductsByCategoryId(id) {
    const res = await axios.get(`${domainName}/api/products/categoryId?categoryId=${id}`)
    const products = res.data.data
    return products
}

export async function getProductByName(name) {
    const res = await axios.get(`${domainName}/api/products/productName?productName=${name}`)
    const product = res.data.data
    return product
}

//categoryes api util
export async function getAllCategoryes() {
  const res = await axios.get(`${domainName}/api/categoryes`);
  const categoryes = await res.data.data;
  return categoryes;
}

export function getCategoryIDByName(categories, categoryName, language) {
    const category = categories.find(category => category[language].toLowerCase() === categoryName.toLowerCase());
    return category ? category._id : null; // Return category ID if found, otherwise null
  }
