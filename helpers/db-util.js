import { MongoClient } from "mongodb";

export async function connectDatabase() {
  const client = await MongoClient.connect(
    "mongodb+srv://poghosyanv30:6moZboPLvS5LtPpp@ost-api.cdroi6r.mongodb.net/"
  );
  return client;
}

export async function insertDocument(client, collection, document) {
  const db = client.db();
  const result = await db.collection(collection).insertOne(document);
  return result;
}

export async function getAllDocuments(client, collection) {
  const db = client.db();

  const documents = await db
    .collection(collection)
    .find()
    .toArray();

  return documents;
}

export async function getDocumentsByFillter(client, collection, fillter) {
    const db = client.db();

    const documents = await db
    .collection(collection)
    .find(fillter)
    .toArray();

    return documents;
}

export async function getDocumentByName(client, collection) {
    const db = client.db();

    const document = await db
      .collection(collection)
      .find()
}
