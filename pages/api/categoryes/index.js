import { connectDatabase, getAllDocuments } from "@/helpers/db-util";

export default async function handler(req, res) {
  let client;

  try {
    client = await connectDatabase();
  } catch (err) {
    res.status(500).json({ message: "Connecting to database failed" });
    return;
  }

  if (req.method === "GET") {
    try {
      const categoryes = await getAllDocuments(client, "categoryes");
      res.status(200).json({ data: categoryes });
    } catch (error) {
      console.log(error.message, "error on get categoryes");
      res.status(500).json({ message: "fetching all categoryes failed" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }

  console.log("client closing");
  client.close();
}
