import axios from "axios";
const botToken = "6117032902:AAEbcIs3bhprQkKdDIVe2Iztd1IAH-eXiHw";
const chatId = "1032263874";

async function handler(req, res) {
  if (req.method === "POST") {
    const { orderData, cartData } = req.body.data;

    if (
      orderData.name.trim() === "" ||
      orderData.address === "" ||
      orderData.phonePrimary === "" ||
      orderData.phonePrimary.length < 9
    ) {
      res.status(422).json({ message: "Please fill all required fields" });
      return;
    }

    let ordersString = '';
    for (let i = 0; i < cartData.items.length; i++) {
      console.log(cartData.items[i], "iteeeem");
      ordersString += `%0A${cartData.items[i].name['am']} - x ${cartData.items[i].amount}`;
    }
    const payment =
      orderData.paymentMethod === "credit-card"
        ? "կրեդիտ քարտով"
        : "կանխիկ դրամով";

    const message = `
    %0AԱնուն: ${orderData.name},
    %0Aհեռախոսահամար: ${orderData.phonePrimary},
    %0Aերկրորդ հեռախոսահամարը: ${orderData.phoneSecondary}
    %0AՀասցե: ${orderData.address},
    %0AՇենք,բնակարան: ${orderData.addressOptional},
    %0AՎճարման եղանակ: ${payment},
    %0A
    ${ordersString}
    %0A
    %0AՆշումներ պատվերի մասին: ${orderData.details},
    %0A
    %0Aընդհանուր գումարը: ${cartData.totalAmount}
    `;

    const url = `https://api.telegram.org/bot${botToken}/sendMessage?chat_id=${chatId}&text=${message}`;

    try {
      await axios.get(url);
    } catch (error) {
      res.status(500).json({
        message:
          "Somthing went wrong , please try again later, connection error",
      });
    }

    res.status(200).json({ message: "Order was sent successfuly" });
  }
}

export default handler;
