import axios from "axios";

async function handler(req, res) {
  if (req.method === "POST") {
    const name = req.body.name;
    const phone = req.body.phone;
    const title = req.body.title;
    const message = req.body.message;

    if (
      !name ||
      name.trim() === "" ||
      !phone ||
      phone.trim() === "" ||
      !title ||
      title.trim() === "" ||
      !message ||
      message.trim() === ""
    ) {
      res.status(422).json({ message: "You have to fill all fields" });
      return;
    }

    const messageBody = `
    %0AԵս ուզում եմ կապ հաստատել
    %0A
    %0AԱնուն: ${name}
    %0Aհեռախոսահամար: ${phone}
    %0AՎերնագիր: ${title}
    %0A
    %0Aհաղորդագրություն: ${message}
    `;

    const botToken = "6117032902:AAEbcIs3bhprQkKdDIVe2Iztd1IAH-eXiHw";
    const chatId = "1032263874";
    const url = `https://api.telegram.org/bot${botToken}/sendMessage?chat_id=${chatId}&text=${messageBody}`;

    try {
      await axios.get(url);
    } catch (error) {
      res.status(500).json({
        message: "Somthing went wrong , please try again later axiosi error",
      });
    }

    res.status(200).json({ message: "Message was sent successfuly" });
  }
}

export default handler;
