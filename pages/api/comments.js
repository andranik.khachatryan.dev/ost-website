import axios from "axios";

async function handler(req, res) {
  if (req.method === "POST") {
    const userComment = req.body.comment;
  
    if (!userComment || userComment.trim() === "") {
      res
        .status(422)
        .json({ message: "Somthing went wrong , please try again later" });
      return;
    }
    const botToken = "6117032902:AAEbcIs3bhprQkKdDIVe2Iztd1IAH-eXiHw";
    const chatId = "1032263874";
    const url = `https://api.telegram.org/bot${botToken}/sendMessage?chat_id=${chatId}&text=${userComment}`;

    try {
      await axios.get(url);
    } catch (error) {
      res
        .status(500)
        .json({
          message: "ISomthing went wrong , please try again later axiosi error",
        });
    }

    res.status(200).json({ message: "Comment was sent successfuly" });
  }
}

export default handler;
