import {
  connectDatabase,
  getDocumentsByFillter,
} from "@/helpers/db-util";

export default async function handler(req, res) {
  let client;
  const { categoryId } = req.query;
  try {
    client = await connectDatabase();
  } catch (err) {
    res.status(500).json({ message: "Connecting to database failed" });
    return;
  }
  if (!categoryId) {
    res.status(500).json({ message: "categoryId is undefined" });
    return;
  }

  if (req.method === "GET") {
    try {
      const products = await getDocumentsByFillter(client, "products", {
        category: categoryId,
      });
      res.status(200).json({ data: products });
    } catch (error) {
      console.log(error.message, "error on get products by category id");
      res
        .status(500)
        .json({ message: "fetching products by category id failed" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }
  client.close();
}
