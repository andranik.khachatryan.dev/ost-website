import {
  connectDatabase,
  getAllDocuments,
} from "@/helpers/db-util";

export default async function handler(req, res) {
  let client;
  try {
    client = await connectDatabase();
  } catch (err) {
    res.status(500).json({ message: "Connecting to database failed" });
    return;
  }

  if (req.method === "GET") {
    try {
      const products = await getAllDocuments(client, "products");
      res.status(200).json({ data: products });
    } catch (error) {
      res.status(500).json({ message: "fetching all products failed" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }

  client.close();
}
