import { connectDatabase, getDocumentsByFillter } from "@/helpers/db-util";

export default async function handler(req, res) {
  let client;
  const { productName } = req.query;
  try {
    client = await connectDatabase();
  } catch (err) {
    res.status(500).json({ message: "Connecting to database failed" });
    return;
  }
  if (!productName) {
    res.status(500).json({ message: "productName is undefined" });
    return;
  }

  if (req.method === "GET") {
    try {
      const products = await getDocumentsByFillter(client, "products", {
        $or: [{ "name.en": productName }, { "name.am": productName }, { "name.ru": productName }],
      });
      const product = products[0]
      console.log(product, 'product');
      res.status(200).json({ data: product });
    } catch (error) {
      console.log(error.message, "error on get product by name");
      res.status(500).json({ message: "fetching product by name failed" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }
  client.close();
}
