import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useLikedContext } from "@/store/liked-context";
import ProductCard from "@/components/ProductElements/card/product-card";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

const LikedPage = () => {
  const { t } = useTranslation("liked");
  const { items, clearList } = useLikedContext();
  const [likedItems, setLikedItems] = useState([]);

  useEffect(() => {
    setLikedItems(items);
  }, [items]);

  const clearListHandler = () => {
    clearList();
  };

  return (
    <section className="py-5">
      <div className="container pt-5">
        <div className="row">
          <div className="col-lg-12">
            <div className="d-flex justify-content-between align-items-center">
              <h5 className="mb-0">{t("liked-title")}</h5>
              <button
                className="btn btn-outline-warning border-2"
                onClick={clearListHandler}
                disabled={likedItems.length === 0}
              >
                {t("delete-all-btn")}
              </button>
            </div>
            <hr />
          </div>
        </div>
        <div className="row g-2 g-md-3">
          {likedItems.length === 0 && (
            <div className="d-flex justify-content-center items-align-center">
              <div className="px-3 py-5 my-5 text-center">
                <p>{t("liked-p1")}</p>
                <p>
                  {t("liked-p2")}
                  <span className="text-danger ms-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      className="bi bi-heart-fill"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fillRule="evenodd"
                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                      ></path>
                    </svg>
                  </span>
                </p>
                <Link className="btn btn-warning" href="/products-category">
                  {t("menu-btn")}
                </Link>
              </div>
            </div>
          )}
          {likedItems.length !== 0 &&
            likedItems.map((i) => (
              <div className="col-6 col-md-4 col-xl-3" key={i.id}>
                <ProductCard
                  name={i.name}
                  price={i.price}
                  imgSrc={i.imgSrc}
                  id={i.id}
                />
              </div>
            ))}
        </div>
      </div>
    </section>
  );
};

export default LikedPage;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "header",
        "liked",
        "footer",
      ])),
      // Will be passed to the page component as props
    },
  };
}
