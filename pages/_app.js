import React from "react";
import Head from "next/head";
import { CartCtxProvider } from "../store/cart-context.jsx";
import { LikedCtxProvider } from "@/store/liked-context.jsx";
import { NotificationContextProvider } from "@/store/notification-context.jsx";
import Layout from "@/components/layouts/layout.jsx";
import { appWithTranslation } from "next-i18next";
import nextI18NextConfig from "../next-i18next.config.js";
import "../styles/custom.scss";


function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Ost Food and Wine</title>
        <meta name="description" content="Dynasty Law Group" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NotificationContextProvider>
        <CartCtxProvider>
          <LikedCtxProvider>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </LikedCtxProvider>
        </CartCtxProvider>
      </NotificationContextProvider>
    </>
  );
}

export default appWithTranslation(App, nextI18NextConfig);
