import React, { useEffect, useState } from "react";
import { useCartContext } from "@/store/cart-context";
import Link from "next/link";
import { useRouter } from "next/router";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Image from "next/image";

const CartPage = () => {
  const { t } = useTranslation("cart");
  const { locale } = useRouter();
  const { items, addItem, removeItem, clearCart, deleteItem, totalAmount } =
    useCartContext();

  const [cartItems, setCartItems] = useState([]);
  const [total, setTotal] = useState([]);

  useEffect(() => {
    setCartItems(items);
    setTotal(totalAmount);
  }, [items, totalAmount]);

  const addHandler = (item) => {
    addItem({
      ...item,
      amount: 1,
    });
  };
  const removeHandler = (id) => {
    removeItem(id);
  };
  const deleteHandler = (id) => {
    deleteItem(id);
  };
  const clearHandler = () => {
    clearCart();
  };

  return (
    <section className="py-5">
      <div className="container pt-5">
        <div className="row">
          <div className="col-lg-7 min-h-550">
            <div className="d-flex justify-content-between align-items-center">
              <h5 className="mb-0">{t("cart-title")}</h5>
              <button
                className="btn btn-outline-warning border-2"
                disabled={cartItems.length === 0}
                onClick={clearHandler}
              >
                {t("delete-all-btn")}
              </button>
            </div>
            <hr />
            {cartItems.length === 0 && (
              <div className="d-flex justify-content-center items-align-center">
                <div className="px-3 py-5 my-5 text-center">
                  <p> {t("cart-p1")}</p>
                  <Link className="btn btn-warning" href="/products-category">
                    {t("menu-btn")}
                  </Link>
                </div>
              </div>
            )}
            {cartItems.length !== 0 && (
              <div className="max-h-550">
                {cartItems.map((item) => (
                  <div className="card p-2 p-md-3 mb-2" key={item.id}>
                    <div className="row g-0">
                      <div className="col-4 col-sm-2 order-sm-1">
                        <div className="position-relative card-img-wraper">
                          <Image
                            src={item.imgSrc}
                            fill
                            sizes="100%"
                            alt="product photo"
                            style={{ objectFit: "cover" }}
                            className="rounded-start"
                          />
                        </div>
                      </div>
                      <div className="col-8 col-sm-4 d-flex align-items-center ms-sm-3 order-3 order-sm-2">
                        <p className="mb-0">{item.name[locale]}</p>
                      </div>
                      <div className="col-3 col-sm-2 d-flex align-items-center ms-1 ms-sm-3 order-4 order-sm-3">
                        <p className="mb-0">{item.price} AMD</p>
                      </div>
                      <div className="col-8 col-sm-3 d-flex align-items-center justify-content-around order-sm-4">
                        <button
                          className="btn btn-sm p-2 btn-warning"
                          onClick={removeHandler.bind(null, item.id)}
                        >
                          -
                        </button>
                        <p className="mb-0">
                          <strong>{item.amount}x</strong>
                        </p>
                        <button
                          className="btn btn-sm p-2 btn-warning"
                          onClick={addHandler.bind(null, item)}
                        >
                          +
                        </button>
                        <button
                          type="button"
                          className="btn btn-outline-danger border-0"
                          onClick={deleteHandler.bind(null, item.id)}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            fill="currentColor"
                            className="bi bi-x-circle-fill"
                            viewBox="0 0 16 16"
                          >
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"></path>
                          </svg>
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>

          <div className="col-lg-5">
            <div className="card rounded-3 h-100">
              <div className="card-body d-flex flex-column justify-content-between">
                <div>
                  <h5 className="text-warning card-title mb-0">
                    {t("your-cart-text")}
                  </h5>
                  <hr />
                </div>

                {cartItems.length !== 0 && (
                  <div className="flex-grow-1">
                    {cartItems.map((item) => (
                      <div
                        className="d-flex justify-content-between border-bottom"
                        key={item.id}
                      >
                        <p className="mb-1">{item.name[locale]}</p>
                        <p className="mb-1">{item.price * item.amount} AMD</p>
                      </div>
                    ))}
                  </div>
                )}

                <div>
                  <hr />
                  {/* <div className="d-flex justify-content-between">
                    <p>
                      <strong className="mb-2">{t("delivery")}</strong>
                    </p>
                    <p>
                      <strong className="mb-2">$1000.00</strong>
                    </p>
                  </div> */}
                  <div className="d-flex justify-content-between">
                    <p>
                      <strong className="mb-2">{t("total")}</strong>
                    </p>
                    <p>
                      <strong className="mb-2">{total} AMD</strong>
                    </p>
                  </div>
                  <Link
                    href="/payment"
                    className="btn btn-warning btn-lg w-100"
                    disabled={cartItems.length === 0}
                  >
                    {t("submit-btn")}
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default CartPage;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "header",
        "cart",
        "footer",
      ])),
      // Will be passed to the page component as props
    },
  };
}
