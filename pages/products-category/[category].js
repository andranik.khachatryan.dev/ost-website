import React from "react";
import Categoryes from "@/components/ProductElements/categoryes/categoryes";
import ProductList from "@/components/ProductElements/productList/product-list.jsx";
import CategoryesMobile from "@/components/ProductElements/categoryes/mobile-categoryes";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import axios from "axios";
import { useRouter } from "next/router";
import { getAllCategoryes, getProductsByCategoryId, getCategoryIDByName } from "@/helpers/api-util";

const ProductsCategory = ({ selectedCategory, products, categoryes }) => {
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>
  }

  return (
    <section className="py-5">
      <div className="container-xxl pt-5">
        <CategoryesMobile categoryes={categoryes} />
        <div className="row">
          <div className="col-3 col-xxl-2 d-none d-lg-block">
            {!categoryes && (
              <div className="pt-5 mt-5 d-flex justify-content-center">
                <div class="spinner-border text-warning">
                  <span class="visually-hidden">Loading...</span>
                </div>
              </div>
            )}
            {categoryes && (
              <Categoryes
                selectedCategory={selectedCategory}
                categoryes={categoryes}
              />
            )}
          </div>
          <div className="col-12 col-lg-9 col-xxl-10">
            {!products && (
              <div className="pt-5 mt-5 d-flex justify-content-center">
                <div class="spinner-border text-warning">
                  <span class="visually-hidden">Loading...</span>
                </div>
              </div>
            )}
            {products && (
              <ProductList items={products} category={selectedCategory} />
            )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductsCategory;

export async function getStaticProps({ params, locale }) {
  const category = params.category;
  const categoryes = await getAllCategoryes();
  const categoryId = getCategoryIDByName(categoryes, category, locale)
  const products = await getProductsByCategoryId(categoryId)

  return {
    props: {
      selectedCategory: category,
      categoryes,
      products,
      ...(await serverSideTranslations(locale, [
        "common",
        "header",
        "product-card",
        "filter",
        "footer",
      ])),
      // Will be passed to the page component as props
    },
    revalidate: 10,
  };
}

export async function getStaticPaths({ locales }) {

  const categoryes = await getAllCategoryes();
 
  const paths = categoryes.flatMap((category) => {
    return locales.map((locale) => {
      return {
        params: {
          category: category[locale],
        },
        locale: locale,
      };
    });
  });

  return {
    paths: paths,
    fallback: true,
  };
}
