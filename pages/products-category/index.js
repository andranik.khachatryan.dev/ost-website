import React, { useState } from "react";
import Categoryes from "@/components/ProductElements/categoryes/categoryes";
import CategoryesMobile from "@/components/ProductElements/categoryes/mobile-categoryes";
import ProductList from "@/components/ProductElements/productList/product-list.jsx";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import axios from "axios";
import { getAllCategoryes, getAllProducts } from "@/helpers/api-util";

const ProductsPage = ({ products, categoryes }) => {
  return (
    <section className="py-5">
      <div className="container-xxl pt-5">
        <CategoryesMobile categoryes={categoryes} />
        <div className="row">
          <div className="col-3 col-xxl-2 d-none d-lg-block">
            <Categoryes categoryes={categoryes} />
          </div>
          <div className="col-12 col-lg-9 col-xxl-10">
            <ProductList items={products} />
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductsPage;

export async function getStaticProps({ locale }) {
  
  const products = await getAllProducts()
  const categoryes = await getAllCategoryes()

  return {
    props: {
      products,
      categoryes,
      ...(await serverSideTranslations(locale, [
        "common",
        "header",
        "product-card",
        "filter",
        "footer",
      ])),
      // Will be passed to the page component as props
    },
  };
}
