import React, { useEffect, useState } from "react";
import Head from "next/head";
import { useCartContext } from "@/store/cart-context";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { useNotificationCtx } from "@/store/notification-context";
import { BsArrowLeft } from "react-icons/bs";
import PaymentForm from "@/components/payment/payment-form";
import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";

const PaymentPage = () => {
  const { t } = useTranslation("cart");
  const { t: tPay } = useTranslation("payment");
  const { showNotification } = useNotificationCtx();
  const { locale } = useRouter()

  // cart items part
  const { items, totalAmount } = useCartContext();
  const [total, setTotal] = useState(0);
  const [cartItems, setCartItems] = useState([]);
  const [formData, setFormData] = useState(null);

  useEffect(() => {
    setCartItems(items);
    setTotal(totalAmount);
  }, [items, totalAmount]);

  const onGetDataHandler = ({
    isNameValid,
    isAddressValid,
    isPhoneValid,
    data,
  }) => {
    if (isNameValid && isAddressValid && isPhoneValid) {
      setFormData(data);
    } else {
      setFormData(null);
    }
  };
  const onSubmitHandler = async () => {
    showNotification({
      title: "Sending comment",
      message: "your comment saving in to the database",
      status: "pending",
    });

    try {
      await axios.post("/api/ordering", {
        data: {
          orderData: formData,
          cartData: { items, totalAmount },
        },
      });

      showNotification({
        title: "Success!",
        message: "Your comment was sent!",
        status: "success",
      });
    } catch (error) {
      showNotification({
        title: "Error!",
        message: error.message || "something went wrong",
        status: "error",
      });
    }
  };

  return (
    <>
      <section className="py-5">
        <div className="container pt-5">
          <div className="row">
            <div className="col-lg-7 mb-4 mb-lg-0 min-h-550">
              <div className="d-flex justify-content-between align-items-center">
                <Link
                  href="/cart"
                  className="btn btn-outline-warning border-2"
                  disabled={cartItems.length === 0}
                >
                  <BsArrowLeft className="me-1" />
                  {tPay("back-to-cart-btn")}
                </Link>
                <h5 className="mb-0">{tPay("payment-text")}</h5>
              </div>
              <hr />
              <PaymentForm onGetData={onGetDataHandler} />
            </div>

            <div className="col-lg-5">
              <div className="card rounded-3 h-100">
                <div className="card-body d-flex flex-column justify-content-between">
                  <h5 className="mx-auto">{tPay("your-order-text")}</h5>
                  <div className="d-flex justify-content-between border-bottom mb-3 pb-2">
                    <h6 className="text-warning card-title mb-0">
                      {tPay("product-text")}
                    </h6>
                    <h6 className="text-warning card-title mb-0">
                      {tPay("subtotal-text")}
                    </h6>
                  </div>

                  {cartItems.length !== 0 && (
                    <div className="flex-grow-1">
                      {cartItems.map((item) => (
                        <div
                          className="d-flex justify-content-between align-items-center border-bottom"
                          key={item.id}
                        >
                          <span className="mb-1">
                            {item.name[locale]} <br />{" "}
                            <strong className="me-1">x</strong> {item.amount}{" "}
                          </span>
                          <span className="mb-1">
                            ${item.price * item.amount}
                          </span>
                        </div>
                      ))}
                    </div>
                  )}

                  <div>
                    <hr />
                    <div className="d-flex justify-content-between">
                      <p>
                        <strong className="mb-2">{t("delivery")}</strong>
                      </p>
                      <p>
                        <strong className="mb-2">$1000.00</strong>
                      </p>
                    </div>
                    <div className="d-flex justify-content-between">
                      <p>
                        <strong className="mb-2">{t("total")}</strong>
                      </p>
                      <p>
                        <strong className="mb-2">${total + 1000}</strong>
                      </p>
                    </div>
                    <button
                      type="button"
                      className="btn btn-warning btn-lg w-100"
                      disabled={!formData}
                      onClick={onSubmitHandler}
                    >
                      {tPay("order-btn-text")}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PaymentPage;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "header",
        "cart",
        "payment",
        "footer",
      ])),
      // Will be passed to the page component as props
    },
  };
}
