import React from "react";
import Categoryes from "@/components/ProductElements/categoryes/categoryes";
import ProductDetails from "@/components/ProductElements/product-details.jsx";
import CategoryesMobile from "@/components/ProductElements/categoryes/mobile-categoryes";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import axios from "axios";
import { useRouter } from "next/router";
import {
  getAllCategoryes,
  getProductByName,
  getAllProducts,
} from "@/helpers/api-util";

const ProductDetail = ({ product, categoryes }) => {
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <section className="py-5">
      <div className="container pt-5">
        <CategoryesMobile categoryes={categoryes} />
        <div className="row">
          <div className="col-2 d-none d-xl-block">
            <Categoryes categoryes={categoryes} />
          </div>
          {!product && <div className="text-center text-danger">Loading</div>}
          {!!product && (
            <div className="col-12 col-xl-10">
              <ProductDetails product={product} />
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default ProductDetail;

export async function getStaticPaths({ locales }) {
  let products = []
  try {
    const res = await fetch('http://localhost:3000/api/products');
    const data = await res.json();
    products = data.data;
    console.log(products[0], 'ddk');
  } catch (err) {
    console.log(err.message, 'errrrrrrr');
  }
  // try {
  //   const res = await axios.get('http://localhost:3000/api/products')
  //   products = res.data.data
  //   console.log(products, 'datadtadat');
  // } catch (err) {
  //   console.log(err.message, "err on generating product paths");
  // }
  console.log(products[1], 'ddk');
  const paths = products.flatMap((product) => {
    return locales.map((locale) => {
      const productName = product.name[locale];
      return {
        params: {
          productname: productName,
        },
        locale: locale,
      };
    });
  });

  return {
    paths: paths,
    fallback: true,
  };
}

export async function getStaticProps({ params, locale }) {
  const productName = params.productname;
  const categoryes = await getAllCategoryes();
  const product = await getProductByName(productName);

  return {
    props: {
      product,
      categoryes,
      ...(await serverSideTranslations(locale, [
        "common",
        "header",
        "product-card",
        "footer",
      ])),
    },
    revalidate: 10,
  };
}
