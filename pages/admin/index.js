import React, { useEffect, useState, useRef } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import axios from "axios";
import CategoryesAdmin from "@/components/admin/categoryes-admin";
import ProductCard from "@/components/ProductElements/card/product-card";
import { useRouter } from "next/router";
import { useNotificationCtx } from "@/store/notification-context";

const AdminPage = () => {
  const { showNotification } = useNotificationCtx();
  const { locale } = useRouter();
  // edit part
  const findInput = useRef(null);
  const [searchItem, setSearchItem] = useState(null);
  const submitFindHandler = (e) => {
    e.preventDefault();
    const findVal = findInput.current.value;
    findProduct(findVal)
      .then((item) => {
        console.log(item, "founded item");
        setSearchItem(item);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const findProduct = async (name) => {
    const response = await axios.get(
      `http://localhost:3001/product/search?name=${name}`
    );
    return response.data[0];
  };

  //create part
  const [categoryes, setCategoryes] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("");
  const price = useRef(null);
  const priority = useRef(null);
  const image = useRef(null);
  const nameAm = useRef(null);
  const nameEn = useRef(null);
  const nameRu = useRef(null);
  const descriptionAm = useRef(null);
  const descriptionEn = useRef(null);
  const descriptionRu = useRef(null);

  const createProductHandler = (e) => {
    e.preventDefault();
    showNotification({
      title: "Creating Product",
      message: "Your product saving in to the database",
      status: "pending",
    });
    if (
      !nameEn.current.value ||
      !nameAm.current.value ||
      !nameRu.current.value ||
      !price.current.value ||
      !image.current.files[0] ||
      !priority.current.value ||
      // !descriptionEn.current.value ||
      // !descriptionAm.current.value ||
      // !descriptionRu.current.value ||
      !selectedCategory.trim()
    ) {
      //setnotification fill all
      console.log("not suvbited");
      return;
    } else {
      const formData = new FormData();
      formData.append("nameEn", nameEn.current.value);
      formData.append("nameAm", nameAm.current.value);
      formData.append("nameRu", nameRu.current.value);
      formData.append("price", price.current.value);
      formData.append("file", image.current.files[0]);
      formData.append("priority", priority.current.value);
      formData.append("descriptionEn", descriptionEn.current.value || '');
      formData.append("descriptionAm", descriptionAm.current.value || '');
      formData.append("descriptionRu", descriptionRu.current.value || '');
      formData.append("categoryId", selectedCategory);

      const createProduct = async (data) => {
        const res = await axios.post(
          "http://localhost:3001/product/create",
          data,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        );
        return res.data;
      };
      createProduct(formData)
        .then((data) => {
          showNotification({
            title: "Success!",
            message: "The new category was created and saved!",
            status: "success",
          });
        })
        .catch((e) => {
          showNotification({
            title: "Error!",
            message: e.message || "Something went wrong",
            status: "error",
          });
        });
    }
  };

  const fetchCategoryes = async () => {
    const res = await axios.get("http://localhost:3001/categoryes");
    return res.data;
  };

  const deleteHandler = (id) => {
    deleteProduct(id)
      .then((deletedItem) => {
        showNotification({
          title: "Success!",
          message: "The product was deleted!",
          status: "success",
        });
        setSearchItem(null)
      })
      .catch((e) => {
        showNotification({
          title: "Error!",
          message: e.message || "Something went wrong",
          status: "error",
        });
      });
  };

  const deleteProduct = async (id) => {
    const res = await axios.delete(`http://localhost:3001/product/${id}`);
    return res.data;
  };

  useEffect(() => {
    fetchCategoryes()
      .then((data) => {
        setCategoryes(data);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return (
    <section className="py-5 mt-5 min-h-100vh">
      <h1 className="fs-3 text-center">Admin Panel</h1>

      <div className="container-fluid px-4">
        <CategoryesAdmin />
        <div className="border border-2 border-warning rounded-2 p-2 mt-4">
          <div className="row">
            <h4 className="text-warning">Products</h4>
            <div className="col-12 col-lg-6">
              <p className="text-center">Edit Product</p>
              <form
                className="input-group input-group-sm mb-3"
                onSubmit={submitFindHandler}
              >
                <input
                  type="text"
                  className="form-control"
                  placeholder="Find by Name"
                  ref={findInput}
                />
                <button className="btn btn-warning" type="submit">
                  Button
                </button>
              </form>
              {searchItem && (
                <div className="p-2 border border-warning rounded">
                  <div className="row">
                    <div className="col-md-6">
                      <img
                        src={searchItem.imageSrc}
                        width={200}
                        height={150}
                      ></img>
                      <div>name AM: {searchItem.name["am"]}</div>
                      <div>name EN: {searchItem.name["en"]}</div>
                      <div>name RU: {searchItem.name["ru"]}</div>
                      <div>price: {searchItem.price}</div>
                      <div>priority: {searchItem.priority}</div>
                      <div>description AM: {searchItem.description["am"]}</div>
                      <div>description EN: {searchItem.description["en"]}</div>
                      <div>description RU: {searchItem.description["ru"]}</div>
                    </div>
                    <div className="col-md-6">
                      <button
                        className="btn btn-danger"
                        onClick={deleteHandler.bind(null, searchItem._id)}
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="col-12 col-lg-6">
              <p className="text-center">Create new Product</p>
              <form onSubmit={createProductHandler}>
                <div className="row">
                  <span className="mb-1">Name</span>
                  <div className="col-12 col-sm-4">
                    <div className="input-group input-group-sm flex-nowrap mb-2">
                      <span className="input-group-text">am</span>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Armenian name"
                        ref={nameAm}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-sm-4">
                    <div className="input-group input-group-sm flex-nowrap mb-2">
                      <span className="input-group-text">en</span>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="English name"
                        ref={nameEn}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-sm-4">
                    <div className="input-group input-group-sm flex-nowrap mb-2">
                      <span className="input-group-text">ru</span>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Russian name"
                        ref={nameRu}
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <span className="mb-1">Price and Priority</span>
                  <div className="col-12 col-sm-6">
                    <div className="input-group input-group-sm flex-nowrap mb-2">
                      <span className="input-group-text">price</span>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="Product price"
                        ref={price}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-sm-6">
                    <div className="input-group input-group-sm flex-nowrap mb-2">
                      <span className="input-group-text">priority</span>
                      <input
                        type="number"
                        className="form-control"
                        placeholder="Product priority"
                        ref={priority}
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <span className="mb-1">Image and Categories</span>
                  <div className="col-12 col-sm-6">
                    <div className="input-group input-group-sm flex-nowrap mb-2">
                      <span className="input-group-text">png img</span>
                      <input
                        type="file"
                        className="form-control"
                        placeholder="Upload file"
                        ref={image}
                      />
                    </div>
                  </div>
                  <div className="col-12 col-sm-6">
                    <div className="input-group input-group-sm mb-3">
                      <label
                        className="input-group-text"
                        htmlFor="selectCategory"
                      >
                        Category
                      </label>
                      <select
                        className="form-select"
                        id="selectCategory"
                        onChange={(e) => setSelectedCategory(e.target.value)}
                        value={selectedCategory}
                      >
                        <option value="">Choose...</option>
                        {categoryes &&
                          categoryes.map((item) => (
                            <option key={item._id} value={item._id}>
                              {`${item.am}--${item.en}--${item.ru}`}
                            </option>
                          ))}
                      </select>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="mb-3">
                    <label htmlFor="textarea-input" className="form-label">
                      Description by Armenian
                    </label>
                    <textarea
                      className="form-control"
                      rows="3"
                      ref={descriptionAm}
                    ></textarea>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="textarea-input" className="form-label">
                      Description by English
                    </label>
                    <textarea
                      className="form-control"
                      rows="3"
                      ref={descriptionEn}
                    ></textarea>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="textarea-input" className="form-label">
                      Description by Russian
                    </label>
                    <textarea
                      className="form-control"
                      rows="3"
                      ref={descriptionRu}
                    ></textarea>
                  </div>
                </div>
                <div className="text-center">
                  <button className="btn btn-warning" type="submit">
                    Create Product
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AdminPage;

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common", "header", "footer"])),
      // Will be passed to the page component as props
    },
  };
}
