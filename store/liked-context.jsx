import React, { createContext, useContext, useEffect, useState } from "react";

// liked context exported by default
const LikedContext = createContext();

// liked context provider returend
export const LikedCtxProvider = ({ children }) => {
  const [likedList, setLikedList] = useState(() => {
    const storedLikedList =
      typeof window !== "undefined" ? localStorage.getItem("liked") : null;
    if (storedLikedList) {
      return JSON.parse(storedLikedList);
    } else {
      return [];
    }
  });

  useEffect(() => {
    localStorage.setItem("liked", JSON.stringify(likedList));
  }, [likedList]);

  const toggleLikedItem = (toggleItem) => {
    setLikedList((prevList) => {
      const itemIndex = prevList.findIndex((item) => item.id === toggleItem.id);
      if (itemIndex === -1) {
        // item not in list, add it
        return [...prevList, toggleItem];
      } else {
        // item already in list, remove it
        const updatedList = prevList.filter(
          (item) => item.id !== toggleItem.id
        );
        return updatedList;
      }
    });
  };
  const clearLikedList = () => {
    setLikedList([]);
  };

  const likedListContext = {
    items: likedList,
    toggleItem: toggleLikedItem,
    clearList: clearLikedList,
  };

  return (
    <LikedContext.Provider value={likedListContext}>
      {children}
    </LikedContext.Provider>
  );
};

export function useLikedContext() {
  return useContext(LikedContext);
}
